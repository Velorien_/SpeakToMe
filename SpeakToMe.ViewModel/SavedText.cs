﻿namespace SpeakToMe.ViewModel
{
    public class SavedText
    {
        public string Title { get; set; }

        public string Content { get; set; }
    }
}
