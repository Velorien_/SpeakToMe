﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Windows.Media.SpeechSynthesis;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace SpeakToMe
{
    public sealed partial class MainPage : Page
    {
        bool isSpeaking;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void FontIcon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (isSpeaking)
            {
                PlayButton.Icon = new SymbolIcon(Symbol.Play);
                PlayButton.Label = "speak";
                Speaker.Stop();
            }
            else
            {
                if(string.IsNullOrEmpty(TextToSpeak.Text) || string.IsNullOrWhiteSpace(TextToSpeak.Text))
                {
                    return;
                }

                PlayButton.Icon = new SymbolIcon(Symbol.Stop);
                PlayButton.Label = "stop";
                await SpeakAsync();
            }

            isSpeaking = !isSpeaking;
        }

        private async Task SpeakAsync()
        {
            using (var speech = new SpeechSynthesizer())
            {
                speech.Voice = VoiceComboBox.SelectedItem as VoiceInformation;
                var voiceStream = await speech.SynthesizeTextToStreamAsync(GetTextToSpeak());
                Speaker.SetSource(voiceStream, voiceStream.ContentType);
                Speaker.Play();
            }
        }

        private async Task<SpeechSynthesisStream> GetStreamAsync()
        {
            using (var speech = new SpeechSynthesizer())
            {
                speech.Voice = VoiceComboBox.SelectedItem as VoiceInformation;
                return await speech.SynthesizeTextToStreamAsync(GetTextToSpeak());
            }
        }

        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            SaveButton.Flyout.Hide();
        }

        private async void SaveFileButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var picker = new FileSavePicker();
            picker.SuggestedFileName = "speech";
            picker.FileTypeChoices.Add("WAV file", new List<string>() { ".wav" });
            picker.SuggestedStartLocation = PickerLocationId.Desktop;
            var file = await picker.PickSaveFileAsync();

            if(file == null)
            {
                return;
            }

            var stream = await GetStreamAsync();

            if (stream.Size > int.MaxValue)
            {
                var md = new MessageDialog("File is too large.", "Error");
                return;
            }

            using (var reader = new BinaryReader(stream.AsStreamForRead()))
            {
                await FileIO.WriteBytesAsync(file, reader.ReadBytes((int)stream.Size));
            }
        }

        private void Speaker_MediaEnded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            PlayButton.Icon = new SymbolIcon(Symbol.Play);
            PlayButton.Label = "speak";
            isSpeaking = false;
        }

        private string GetTextToSpeak()
        {
            string ttr;
            if(!string.IsNullOrWhiteSpace(TextToSpeak.SelectedText))
            {
                ttr = TextToSpeak.SelectedText;
            }

            ttr = TextToSpeak.Text;

            //TODO: some formatting

            return ttr;
        }
    }
}
